package banking;

import org.shadowbot.osrs.api.Job;
import org.shadowbot.osrs.api.methods.data.Bank;
import org.shadowbot.osrs.api.methods.data.Inventory;
import org.shadowbot.osrs.api.methods.interactive.Players;
import org.shadowbot.osrs.api.util.Condition;
import org.shadowbot.osrs.api.util.Time;
import storage.Variables;

/**
 * Made using IntelliJ IDEA.
 * User: Spartan 117
 * Date: 1/11/14
 * Time: 1:12 PM
 * Created with Intelligence and Innovation
 */
public class Banking implements Job {

    @Override
    public boolean isActive() {
        return !Inventory.contains("Flax") && !Bank.isOpen() && Variables.BANK_AREA.contains(Players.getLocal().getLocation());
    }

    @Override
    public void run() {
        Variables.status = "Banking for Flax";
        System.out.println("Banking for Flax");
        Bank.open();
        Time.sleep(new Condition() {
            @Override
            public boolean active() {
                return Bank.isOpen();
            }
        }, 1500);
        if (Inventory.contains(1777)) {
            Variables.status = "Depositing Bow String";
            System.out.println("Depoisting Bow String");
            Bank.deposit(1777, 0);
            Time.sleep(new Condition() {
                @Override
                public boolean active() {
                    return !Inventory.contains(1777);
                }
            }, 1500);
        }
        Variables.status = "Withdrawing Flax";
        System.out.println("Withdrawing Flax");
        Bank.withdraw("Flax", 0);
        Time.sleep(new Condition() {
            @Override
            public boolean active() {
                return Inventory.contains("Flax");
            }
        }, 1500);
        Bank.close();
        Time.sleep(new Condition() {
            @Override
            public boolean active() {
                return !Bank.isOpen();
            }
        }, 1500);


    }
}
