package walking;


import org.shadowbot.osrs.api.Job;
import org.shadowbot.osrs.api.methods.data.Inventory;
import org.shadowbot.osrs.api.methods.data.movement.Camera;
import org.shadowbot.osrs.api.methods.input.Mouse;
import org.shadowbot.osrs.api.methods.interactive.GameEntities;
import org.shadowbot.osrs.api.methods.interactive.Players;
import org.shadowbot.osrs.api.util.Random;
import org.shadowbot.osrs.api.util.Time;
import org.shadowbot.osrs.api.wrapper.GameObject;
import org.shadowbot.osrs.api.wrapper.Tile;
import storage.Variables;

/**
 * Made using IntelliJ IDEA.
 * User: Spartan 117
 * Date: 1/11/14
 * Time: 1:43 PM
 * Created with Intelligence and Innovation
 */
public class BankWalk implements Job {
    private GameObject ladder = GameEntities.getNearest(25938);
    private GameObject door = GameEntities.getNearest("Door");
    private GameObject object2 = GameEntities.getNearest("Door", "Ladder");

    @Override
    public boolean isActive() {
        return !Inventory.contains("Flax") && !Variables.BANK_AREA.contains(Players.getLocal().getLocation()) && Players.getLocal().getLocation().getPlane() == 1;
    }

    @Override
    public void run() {

        Camera.turnTo(ladder);
        for (Tile t : Variables.PATH) {
            if (ladder != null && ladder.isOnScreen() && Players.getLocal().getLocation().getPlane() == 1) {
            System.out.println("Interacting with Ladder");
            Variables.status = "Interacting with Ladder";
            ladder.interact("Climb-down");
            System.out.println("On the bottom floor");
            Time.sleep(1000, 2000);
            if (door != null && door.isOnScreen() && Players.getLocal().getLocation().getPlane() == 0) {
                Variables.status = "Interacting with Door";
                System.out.println("Interacting with Door");
                door.interact("Open");
                System.out.println("Door is now open");
                Time.sleep(1000, 2000);
            }
        } else {
                if (t != null && t.isWalkable()) {
                    Variables.status = "Walking 2 Bank";
            System.out.println("Walking 2 Bank");

        }
            }
            lookHuman();
        }
    }

    private void lookHuman() {

        switch (Random.nextInt(0, 50)) {
            case 1:
                if (object2 != null) Camera.turnTo(object2);
                break;

            case 3:
            case 4:
            case 6:
            case 13:
                if (object2 != null) Mouse.move(object2.getPointOnScreen());
                break;

            default:
                break;

        }

    }

}

