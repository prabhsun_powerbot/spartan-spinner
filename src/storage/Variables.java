package storage;


import org.shadowbot.osrs.api.wrapper.Area;
import org.shadowbot.osrs.api.wrapper.Tile;

/**
 * Made using IntelliJ IDEA.
 * User: Spartan 117
 * Date: 1/11/14
 * Time: 1:20 PM
 * Created with Intelligence and Innovation
 */
public class Variables {
    public static String status;
    public static final int MAIN_WIDGET = 459;
    public static final int CHILD_WIDGET = 90;
    public static final Area BANK_AREA = new Area(new Tile(2721, 3493), new Tile(2731, 3489));
    public static final Area SPINNER_AREA = new Area(new Tile(2711, 3470, 1), new Tile(2716, 3473, 1));
    public static final Tile[] PATH = new Tile[]{new Tile(2711, 3471, 1), new Tile(2714, 3470, 1), new Tile(2715, 3472, 0), new Tile(2719, 3472, 0), new Tile(2724, 3478, 0)
            , new Tile(2725, 3484, 0), new Tile(2725, 3493, 0)};
}
