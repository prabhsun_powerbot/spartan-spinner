/**
 * Made using IntelliJ IDEA.
 * User: Spartan 117
 * Date: 1/11/14
 * Time: 1:16 PM
 * Created with Intelligence and Innovation
 */


import org.shadowbot.osrs.api.Job;
import org.shadowbot.osrs.api.methods.data.Game;
import org.shadowbot.osrs.api.util.Random;

import java.util.ArrayList;

public class JobHandler {

    private static ArrayList<Job> jobs = new ArrayList<Job>();

    public static Job[] jobs() {
        return jobs.toArray(new Job[jobs.size()]);
    }

    public static void addJob(Job job) {
        jobs.add(job);
    }

    public static int run() {
        if (Game.isLoggedIn()) {
            for (Job job : jobs) {
                if (job != null && job.isActive()) {
                    job.run();
                }
            }
        }
        return Random.nextInt(150, 200);
    }
}
