import banking.Banking;
import org.shadowbot.osrs.api.Manifest;
import org.shadowbot.osrs.api.ShadowScript;
import org.shadowbot.osrs.api.SkillCategory;
import org.shadowbot.osrs.api.listeners.PaintListener;
import org.shadowbot.osrs.api.methods.input.Mouse;
import org.shadowbot.osrs.api.util.Random;
import spinning.Spinning;
import storage.Variables;
import walking.BankWalk;
import walking.SpinnerWalk;

import java.awt.*;

/**
 * Made using IntelliJ IDEA.
 * User: Spartan 117
 * Date: 1/11/14
 * Time: 1:36 PM
 * Created with Intelligence and Innovation
 */
@Manifest(author = "Spartan_117", vip = false, scriptName = "Spartan Spinner", category = SkillCategory.CRAFTING, description = "Spins flax in Seers Village")
public class SpartanSpinner extends ShadowScript implements PaintListener {
    public static long startTime;

    @Override
    public void render(Graphics graphics) {
        Graphics2D g2 = (Graphics2D) graphics;
        Graphics2D g = (Graphics2D) graphics;
        g.setColor(Color.CYAN);
        g.drawString("" + Variables.status, 30, 30);
        g.drawString("Run Time: " + (System.currentTimeMillis() - startTime), 30, 50);
        g.fillRect(Mouse.getPressX(), Mouse.getPressY(), 2, 2);

    }

    @Override
    public void onStop() {

    }

    @Override
    public int operate() {
        return JobHandler.run();
    }

    @Override
    public void onStart() {
        Mouse.setSpeed((Random.nextInt(50, 60)));
        Variables.status = "Script Started";
        System.out.println("Script Started");
        JobHandler.addJob(new Banking());
        JobHandler.addJob(new SpinnerWalk());
        JobHandler.addJob(new Spinning());
        JobHandler.addJob(new BankWalk());
        startTime = System.currentTimeMillis();


    }
}
