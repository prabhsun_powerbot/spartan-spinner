package spinning;

import org.shadowbot.osrs.api.Job;
import org.shadowbot.osrs.api.methods.data.Inventory;
import org.shadowbot.osrs.api.methods.input.Keyboard;
import org.shadowbot.osrs.api.methods.interactive.GameEntities;
import org.shadowbot.osrs.api.methods.interactive.Players;
import org.shadowbot.osrs.api.methods.interactive.Widgets;
import org.shadowbot.osrs.api.util.Random;
import org.shadowbot.osrs.api.util.Time;
import org.shadowbot.osrs.api.util.Timer;
import org.shadowbot.osrs.api.wrapper.GameObject;
import storage.Variables;

/**
 * Made using IntelliJ IDEA.
 * User: Spartan 117
 * Date: 1/11/14
 * Time: 1:44 PM
 * Created with Intelligence and Innovation
 */
public class Spinning implements Job {
    private GameObject loaded = GameEntities.getNearest(25824);
    private Timer t = new Timer(2000);

    @Override
    public boolean isActive() {
        return Inventory.contains("Flax") && Variables.SPINNER_AREA.contains(Players.getLocal().getLocation()) && !Players.getLocal().isMoving() && Players.getLocal().getLocation().getPlane() == 1;
    }

    @Override
    public void run() {
        Variables.status = "Interacting with Spinner";
        System.out.println("Running Spinning");

        if (loaded != null && loaded.isOnScreen() && Widgets.get(Variables.MAIN_WIDGET, Variables.CHILD_WIDGET) == null) {
            Variables.status = "Interacting with the Spinning Wheel";
            System.out.println("Interacting with the Spinning Wheel");
            if (!loaded.interact("Spin")) {
                System.out.println("failed");
                return;
            }

        }
        if (Widgets.get(Variables.MAIN_WIDGET, Variables.CHILD_WIDGET) != null && Widgets.get(Variables.MAIN_WIDGET, Variables.CHILD_WIDGET).isVisible()) {
            Variables.status = "Making Bow Strings";
            System.out.println("Making Bow Strings");
            Widgets.get(Variables.MAIN_WIDGET, Variables.CHILD_WIDGET).interact("Make X");
            Time.sleep(1000, 2000);
            Keyboard.sendText("" + Random.nextInt(30, 60), true);
            while (t.isRunning()) {
                if ((Players.getLocal().getAnimation() != -1)) {
                    t = new Timer(2000);
                }
            }


        }
    }
}
